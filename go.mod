module gitlab.com/blinds1de/amqpconnector

go 1.15

require (
	github.com/rabbitmq/amqp091-go v1.9.0
	github.com/sirupsen/logrus v1.9.3
)
