package amqpconnector

import (
	"context"
	"strconv"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	log "github.com/sirupsen/logrus"
)

/*
Config contains the configuration Data that is necessary for the amqpConnector to create
the necessary exchanges, queues and bindings
*/
type Config struct {
	AmqpUser             string
	AmqpPassword         string
	AmqpPort             int
	AmqpURI              string
	ExchangeDeclarations []ExchangeDeclaration
	QueueDeclarations    []QueueDeclaration
	BindingDeclarations  []BindingDeclaration
}

// ExchangeDeclaration contains data necessary for the amqpConnector to declare an exchange on its server
type ExchangeDeclaration struct {
	ExchangeType string
	ExchangeName string
	Publisher    string
	Consumers    []Consumer
}

// QueueDeclaration contains data necessary for the amqpConnector to declare a queue on its server
type QueueDeclaration struct {
	QueueName string
	Consumer  Consumer
	Publisher string
}

// BindingDeclaration contains data necessary for the amqpConnector to declare a binding on its server
type BindingDeclaration struct {
	QueueName    string
	ExchangeName string
	RoutingKey   string
}

// PublishMessage is a wrapper for the channel to publishers which also contains the RoutingKey as information
// as this is dynamically necessary to publish properly
type PublishMessage struct {
	PubMessage amqp.Publishing
	RoutingKey string
}

type Consumer struct {
	ConsumerName string
	QueueName    string
	RoutingKey   []string
}

type consumer struct {
	Name         string
	Channel      chan amqp.Delivery
	QueueName    string
	AutoBind     bool
	ExchangeName string
}

type exchangePublisher struct {
	Name         string
	Channel      chan PublishMessage
	ExchangeName string
}

type queuePublisher struct {
	Name      string
	Channel   chan PublishMessage
	QueueName string
}

// GoAmqp spins up the handling of the amqp connections, opens the connections / channels, creates Exchanges Queues and Bindings and reads from Publisher
// Channels and pushes onto amqp, reading from amqp and pushing to consumer channels
func GoAmqp(ctx context.Context, config *Config) (map[string]chan PublishMessage, map[string]chan amqp.Delivery) {
	publishers := make(map[string]chan PublishMessage)
	consumers := make(map[string]chan amqp.Delivery)

	internalQueuePublishers := make([]queuePublisher, 0)
	internalExchangePublishers := make([]exchangePublisher, 0)
	internalConsumer := make([]consumer, 0)

	for _, ex := range config.ExchangeDeclarations {
		if ex.Publisher != "" {
			pubChan := make(chan PublishMessage)
			publishers[ex.Publisher] = pubChan
			internalExchangePublishers = append(internalExchangePublishers, exchangePublisher{
				Name:         ex.Publisher,
				Channel:      pubChan,
				ExchangeName: ex.ExchangeName,
			})
		}

		for _, exCon := range ex.Consumers {
			conChan := make(chan amqp.Delivery)
			consumers[exCon.ConsumerName] = conChan
			internalConsumer = append(internalConsumer, consumer{
				Name:      exCon.ConsumerName,
				Channel:   conChan,
				QueueName: exCon.QueueName,
			})
		}
	}
	for _, qu := range config.QueueDeclarations {
		if qu.Consumer.ConsumerName != "" {
			conChan := make(chan amqp.Delivery)
			consumers[qu.Consumer.ConsumerName] = conChan
			internalConsumer = append(internalConsumer, consumer{
				Name:      qu.Consumer.ConsumerName,
				Channel:   conChan,
				QueueName: qu.QueueName,
			})
		}
		if qu.Publisher != "" {
			pubChan := make(chan PublishMessage)
			publishers[qu.Publisher] = pubChan
			internalQueuePublishers = append(internalQueuePublishers, queuePublisher{
				Name:      qu.Publisher,
				Channel:   pubChan,
				QueueName: qu.QueueName,
			})
		}
	}
	go func(ctx context.Context, config *Config, internalQueuePublishers []queuePublisher, internalExchangePublishers []exchangePublisher, internalConsumer []consumer) {
		for {
			err := startAmqpHandling(ctx, config, internalQueuePublishers, internalExchangePublishers, internalConsumer)
			if err == nil {
				log.Info("Shutting down AMQP handling due to context cancellation.")
				return
			}
			log.Error(err)
			time.Sleep(2 * time.Second)
		}
	}(ctx, config, internalQueuePublishers, internalExchangePublishers, internalConsumer)
	return publishers, consumers
}

func startAmqpHandling(ctx context.Context, config *Config, internalQueuePublishers []queuePublisher,
	internalExchangePublishers []exchangePublisher, internalConsumer []consumer) error {
	log.Info("Starting AmqpHandling")
	localCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	amqpConString := "amqp://" + config.AmqpUser + ":" + config.AmqpPassword + "@" + config.AmqpURI + ":" + strconv.Itoa(config.AmqpPort) + "/"
	conn, err := amqp.Dial(amqpConString)
	defer conn.Close()
	if err != nil {
		return err
	}
	connCloseListener := make(chan *amqp.Error)
	conn.NotifyClose(connCloseListener)

	ch, err := conn.Channel()
	defer ch.Close()
	if err != nil {
		return err
	}

	autoQueues := make([]QueueDeclaration, 0)
	autoBindings := make([]BindingDeclaration, 0)

	for _, exDecl := range config.ExchangeDeclarations {
		if err = declareExchange(ch, exDecl); err != nil {
			return err
		}

		for _, exCon := range exDecl.Consumers {
			autoQueues = append(autoQueues, QueueDeclaration{
				QueueName: exCon.QueueName,
			})

			for _, rk := range exCon.RoutingKey {
				autoBindings = append(autoBindings, BindingDeclaration{
					QueueName:    exCon.QueueName,
					ExchangeName: exDecl.ExchangeName,
					RoutingKey:   rk,
				})
			}
		}
	}

	for _, qDecl := range config.QueueDeclarations {
		if err = declareQueue(ch, qDecl); err != nil {
			return err
		}
	}

	for _, qDecl := range autoQueues {
		if err = declareQueue(ch, qDecl); err != nil {
			return err
		}
	}

	for _, bDecl := range config.BindingDeclarations {
		if err = declareBinding(ch, bDecl); err != nil {
			return err
		}
	}

	for _, bDecl := range autoBindings {
		if err = declareBinding(ch, bDecl); err != nil {
			return err
		}
	}

	for _, publisher := range internalExchangePublishers {
		startPublishers(localCtx, ch, publisher.ExchangeName, publisher.Channel, false)
	}

	for _, publisher := range internalQueuePublishers {
		startPublishers(localCtx, ch, publisher.QueueName, publisher.Channel, true)
	}

	for _, consumer := range internalConsumer {
		if err = startConsumer(localCtx, ch, consumer.QueueName, consumer.Channel); err != nil {
			return err
		}
	}

	select {
	case <-ctx.Done():
		return nil
	case err = <-connCloseListener:
		return err
	}
}

func startPublishers(ctx context.Context, ch *amqp.Channel, destination string, channel chan PublishMessage, isQueuePublisher bool) {
	go func(ctx context.Context, ch *amqp.Channel, destination string, channel chan PublishMessage, isQueuePublisher bool) {
		exchange := destination
		if isQueuePublisher {
			exchange = ""
		}
		for {
			select {
			case <-ctx.Done():
				return
			case pubMsg := <-channel:
				routingKey := pubMsg.RoutingKey
				if isQueuePublisher {
					routingKey = destination
				}
				err := ch.Publish(
					exchange,   // exchange
					routingKey, // routing key
					false,      // mandatory
					false,      // immediate
					pubMsg.PubMessage,
				)
				if err != nil {
					log.Error(err)
				}
			}
		}
	}(ctx, ch, destination, channel, isQueuePublisher)
}

func startConsumer(ctx context.Context, ch *amqp.Channel, queueName string, channel chan amqp.Delivery) error {
	msgs, err := ch.Consume(
		queueName, // queue
		"",        // consumer
		true,      // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
	if err != nil {
		return err
	}
	go func(ctx context.Context, consumeChannel <-chan amqp.Delivery, channel chan amqp.Delivery) {
		for {
			select {
			case <-ctx.Done():
				return
			case amqpMsg := <-msgs:
				channel <- amqpMsg
			}
		}
	}(ctx, msgs, channel)
	return nil
}

func declareBinding(ch *amqp.Channel, bindingDeclaration BindingDeclaration) error {
	if err := ch.QueueBind(
		bindingDeclaration.QueueName,    // queue name
		bindingDeclaration.RoutingKey,   // routing key
		bindingDeclaration.ExchangeName, // exchange
		false,
		nil,
	); err != nil {
		return err
	}
	return nil
}

func declareQueue(ch *amqp.Channel, queueDeclaration QueueDeclaration) error {
	_, err := ch.QueueDeclare(
		queueDeclaration.QueueName, //name
		true,                       //durable
		false,                      //delete when usused
		false,                      //exclusive
		false,                      //no-wait
		nil,                        //arguments
	)
	if err != nil {
		return err
	}
	return nil
}

func declareExchange(ch *amqp.Channel, exchangeDeclaration ExchangeDeclaration) error {
	err := ch.ExchangeDeclare(
		exchangeDeclaration.ExchangeName, // name
		exchangeDeclaration.ExchangeType, // type
		true,                             // durable
		false,                            // auto-deleted
		false,                            // internal
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return err
	}
	return nil
}
