package amqpconnector

import (
	"context"
	"testing"
)

func TestSum(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	config := &Config{
		ExchangeDeclarations: []ExchangeDeclaration{
			{
				ExchangeType: "topic",
				ExchangeName: "testExchange.Test",
				Publisher:    "publisher1",
				Consumers: []Consumer{{
					ConsumerName: "testAutoConsumer",
					QueueName:    "testAutoQueue",
					RoutingKey: []string{
						"testRK",
						"testRK2",
						"testRK3",
					},
				}},
			},
			{
				ExchangeType: "topic",
				ExchangeName: "testExchange.Test2",
				Publisher:    "publisher2",
			},
			{
				ExchangeType: "topic",
				ExchangeName: "testExchange.Test3",
			},
		},
		QueueDeclarations: []QueueDeclaration{
			{
				QueueName: "testqueue",
				Consumer: Consumer{
					ConsumerName: "test1consumer",
				},
			},
			{
				QueueName: "testqueue2",
				Publisher: "queuePublisher1",
				Consumer: Consumer{
					ConsumerName: "test2consumer",
				},
			},
			{
				QueueName: "testqueue",
			},
		},
	}
	publishers, consumers := GoAmqp(ctx, config)
	if len(publishers) != 3 {
		t.Errorf("Unexpected publisher length, got %d - wanted %d", len(publishers), 3)
	}
	if len(consumers) != 3 {
		t.Errorf("Unexpected consumers length, got %d - wanted %d", len(consumers), 3)
	}
}
